# TODO-APP

### Description
This application has multiple branches designated with `000-description` example is `015-simple-HTTP`

The higher the number the more advanced the concept. You can swith through those branches to review your concepts

### Installation
``` 
npm install
```

### Running 
```
npm run start
```

###  Dependency
- Built on node version 12.9.1
